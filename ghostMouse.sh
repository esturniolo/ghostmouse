#!/bin/bash

xdotool &> /dev/null

RETURNCODE=$(echo $?)

if [ $RETURNCODE = 127 ]; then
    echo ""
    echo "The script will ask you for your 'sudo' credentials to install 'xdotool'"
    sleep 7
    echo ""
    echo "Installing proper dependencies."
    sudo apt install xdotool -y &> /dev/null
    echo ""
    echo "All set! Enjoy!"
    sleep 5
fi

clear 

echo " -----------------------"
echo "| Ghost mouse enabled... |"
echo " -----------------------"
echo ""
echo "Press CTRL +C to stop."

while true
do
    xdotool mousemove_relative 1 -1
    sleep 60
done
